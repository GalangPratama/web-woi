$(document).ready(function () {

    $(function() {
      var start = moment().subtract(29, 'days');
      var end = moment();
  
      function cb(start, end) {
          $('#reporthapus span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
  
  
        $('#reporthapus').daterangepicker({
          startDate: start,
          endDate: end,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
        }, cb);
  
        cb(start, end);
  
    });
  
      var dataopen = $('#absen').DataTable({
          "oLanguage": {
              "sSearch": "Filter Data" //Will appear on search form
            },
          "ordering": false,
          "ajax":{  
            url:"./absen/absen_user",  
          },  
          columns: [
          { "data": null },
          { "data": "jdNoDelivery" }, 
          { "data": null,
          render:function(data, type, row)
          {
            return moment(data['jobDate']).format('DD MMM YYYY');
          }
          },
          { "data": "jobCourier" }, 
          { "data": null,
          render:function(data, type, row)
          {
            return moment(data['Masuk']).format('H:m');
          }
          },
          { "data": null,
          render:function(data, type, row)
          {
            return moment(data['Pulang']).format('H:m');
          }
          }
        //   { "data": null,
        //   render:function(data, type, row)
        //   {
        //     return '<button type="button" name="modal_image" id="'+ data['jdRef'] +'" class="modal_image btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
        //   }
        // }
        ], 
      });
      dataopen.on( 'order.dt search.dt', function () {
        dataopen.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
      } ).draw();
  
      $('#table-filter').on('change', function(){
        dataopen.search(this.value).draw();   
      });
  
    $('#reporthapus').on('apply.daterangepicker', function(ev, picker) {
      var start = picker.startDate;
      var end = picker.endDate;
  
        $.fn.dataTable.ext.search.push(
        function(settings, data, dataIndex) {
          var min = start;
          var max = end;
          var startDate = new Date(data[3]);
          
          if (min == null && max == null) {
            return true;
          }
          if (min == null && startDate <= max) {
            return true;
          }
          if (max == null && startDate >= min) {
            return true;
          }
          if (startDate <= max && startDate >= min) {
            return true;
          }
          return false;
        }
      );
      dataopen.draw();
      $.fn.dataTable.ext.search.pop();
    });
});

// $(document).on('click', '.modal_image', function(){  
//     var user_id = $(this).attr("id");  
//       $.ajax({  
//            url:"/WOI/laporan/fetch_single_user",  
//            method:"POST",
//            data:{user_id:user_id},  
//            dataType:"json",  
//            success:function(data)  
//            {  
//                 $('#tesModal').modal('show');  
//                 $('#jobRef').text(data.jobRef);
//                 $('#jdNoDelivery').text(data.jdNoDelivery);
//                 $('#jdConsignee').text(data.jobCourier);
//                 $('#jdReceiver').text(data.jdReceiver);
//                 $('#jdReceiverTitle').text(data.jdReceiverTitle);
//                 $('#customerAddress').text(data.customerAddress);
//                 $('#jdGoodsType').text(data.typeId);
//                 $('#jdQty').text(data.jdQty);
//                 $('#jdNote').text(data.jdNote);
//                 $('#jdStatus').text(data.jdStatus);
//                 $('#jdimg1').html(data.Image1);
//                 $('#jdimg2').html(data.Image2);
//                 $('#jdimg3').html(data.Image3);
//                 $('#jdimg4').html(data.Image4);
//                 $('.modal-title #jdRef').text(user_id);
//                 $('.modal-title #jdUpdated').text(data.jdUpdated);  
//            } 
//  });

// }); 

$(".reporthapus").on('apply.daterangepicker', function (ev, picker) {

    ev.preventDefault();



    //if blank date option was selected
    if ((picker.startDate.format('DD-MMM-YYYY') == "01-Jan-0001") && (picker.endDate.format('DD-MMM-YYYY')) == "01-Jan-0001") {
        $(this).val('Blank');


        val = "^$";

        dataTable.column(dataIdx)
           .search(val, true, false, true)
           .draw();

    }
    else {
        //set field value
        $(this).val(picker.startDate.format('DD-MMM-YYYY') + ' to ' + picker.endDate.format('DD-MMM-YYYY'));



        //run date filter
        startDate = picker.startDate.format('DD-MMM-YYYY');
        endDate = picker.endDate.format('DD-MMM-YYYY');

        var dateStart = parseDateValue(startDate);
        var dateEnd = parseDateValue(endDate);

        var filteredData = dataTable
                .column(dataIdx)
                .data()
                .filter(function (value, index) {

                    var evalDate = value === "" ? 0 : parseDateValue(value);
                    if ((isNaN(dateStart) && isNaN(dateEnd)) || (evalDate >= dateStart && evalDate <= dateEnd)) {

                        return true;
                    }
                    return false;
                });


        var val = "";
        for (var count = 0; count < filteredData.length; count++) {

            val += filteredData[count] + "|";
        }

        val = val.slice(0, -1);


        dataTable.column(dataIdx)
              .search(val ? "^" + val + "$" : "^" + "-" + "$", true, false, true)
              .draw();
    }


   

});

$(".daterange").on('cancel.daterangepicker', function (ev, picker) {
    ev.preventDefault();
    $(this).val('');
    dataTable.column(dataIdx)
          .search("")
          .draw();
});
