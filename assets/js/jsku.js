<<<<<<< HEAD

// Datatables Laporan Delivered
$(document).ready(function () {

  $(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }


      $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }, cb);

      cb(start, end);

    });

  var dataku = $('#order_data').DataTable({
      "oLanguage": {
          "sSearch": "Filter Data" //Will appear on search form
        },
        dom: 'lrtip',
        dom: 'Bfrtip',
        buttons: [
        {
          extend: 'excel',
          text: 'EXPORT EXCEL',
          autoFilter: true,
          exportOptions: {
            columns:  [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 12, 14]
          }               
        }
        ],
        columnDefs:[
        {  
        "targets":[1, 2, 6, 8, 9, 11, 12, 13 ],  
        "visible": false  
      }
      ],
      "ordering" : false,
      "ajax":{  
        url:"../laporan/fetch_user_delivered",  
      },  
      columns: [
      { "data": null },
      { "data": "jdRef" },
      { "data": "jobRef" },
      { "data": "jdNoDelivery" },
      { "data": null,
      render:function(data, type, row)
      {
        return moment(data['jdCreated']).format('DD-MMM-YYYY H:m:ss');
      }
      },
      { "data": "jobArea" },
      { "data": "plat" },
      { "data": "jobCourier" },
      { "data": "jdReceiver" },
      { "data": "customerAddress" },
      { "data": "customerName" },
      { "data": "type_of_goods" },
      { "data": "jdQty" },
      { "data": "jdNote" },
      { "data": "jdStatus" },
      { "data": null,
      render:function(data, type, row)
      {
        return '<button type="button" name="modal_image" id="'+ data['jdRef'] +'" class="modal_image btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
      }
    },
    { "data": null,
    render:function(data, type, row)
    {
      return '<a href="../laporan/hasil/'+ data['jdRef'] +'" target="_blank" type="button" name="modal_pdf" class="modal_pdf btn btn-default btn-xs">PDF</a>';
    }}
    ],  
  });

  dataku.on( 'order.dt search.dt', function () {
    dataku.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
  } ).draw();

=======
$(document).ready(function () {

$(function() {
  var start = moment().subtract(29, 'days');
  var end = moment();

  function cb(start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }


    $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, cb);

    cb(start, end);

  });

   $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
   var start = picker.startDate;
   var end = picker.endDate;

    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
      var min = start;
      var max = end;
      var startDate = new Date(data[3]);
      
      if (min == null && max == null) {
        return true;
      }
      if (min == null && startDate <= max) {
        return true;
      }
      if (max == null && startDate >= min) {
        return true;
      }
      if (startDate <= max && startDate >= min) {
        return true;
      }
      return false;
    }
  );
  dataku.draw();
  $.fn.dataTable.ext.search.pop();
});

  var dataku = $('#order_data').DataTable({
    "oLanguage": {
        "sSearch": "Filter Data" //Will appear on search form
      },
      dom: 'lrtip',
      dom: 'Bfrtip',
      buttons: [
      {
        extend: 'excel',
        text: 'EXPORT EXCEL',
        autoFilter: true,
        exportOptions: {
          columns:  [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 12]
        }               
      }
      ],
      columnDefs:[
      {  
       "targets":[ 1, 5, 7, 8, 10, 11, 12 ],  
       "visible": false  
     }
     ],
     "ordering": false,
     "ajax":{  
      url:"/WOI/laporan/fetch_user",  
    },  
    columns: [
    { "data": "jdRef" },
    { "data": "jobRef" },
    { "data": "jdNoDelivery" },
    { "data": null,
    render:function(data, type, row)
    {
      return moment(data['jdCreated']).format('DD-MMM-YYYY H:m:ss');
    }
    },
    { "data": "jobArea" },
    { "data": "jobNopol" },
    { "data": "jobCourier" },
    { "data": "jdReceiver" },
    { "data": "customerAddress" },
    { "data": "customerName" },
    { "data": "jdGoodsType" },
    { "data": "jdQty" },
    { "data": "jdNote" },
    { "data": "jdStatus" },
    { "data": null,
    render:function(data, type, row)
    {
      return '<button type="button" name="modal_image" id="'+ data['jdRef'] +'" class="modal_image btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
    }
  },
  { "data": null,
  render:function(data, type, row)
  {
    return '<a href="laporan/hasil/'+ data['jdRef'] +'" target="_blank" type="button" name="modal_pdf" class="modal_pdf btn btn-default btn-xs">PDF</a>';
  }}
  ],  
});
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3
  $('#table-filter').on('change', function(){
    dataku.search(this.value).draw();   
  });


  $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
   var start = picker.startDate;
   var end = picker.endDate;

    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
      var min = start;
      var max = end;
<<<<<<< HEAD
      var startDate = new Date(data[4]);
=======
      var startDate = new Date(data[3]);
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3
      
      if (min == null && max == null) {
        return true;
      }
      if (min == null && startDate <= max) {
        return true;
      }
      if (max == null && startDate >= min) {
        return true;
      }
      if (startDate <= max && startDate >= min) {
        return true;
      }
      return false;
    }
<<<<<<< HEAD
    );
    dataku.draw();
    $.fn.dataTable.ext.search.pop();
  });
});
//End Laporan

// Datatables Laporan Open
$(document).ready(function () {

  $(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reportopen span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }


      $('#reportopen').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }, cb);

      cb(start, end);

  });

    var dataopen = $('#order_open').DataTable({
        "oLanguage": {
            "sSearch": "Filter Data" //Will appear on search form
          },
          dom: 'lrtip',
          dom: 'Bfrtip',
          buttons: [
          {
            extend: 'excel',
            text: 'EXPORT EXCEL',
            autoFilter: true,
            exportOptions: {
              columns:  [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 12, 14]
            }               
          }
          ],
          columnDefs:[
          {  
            "targets":[1, 2, 6, 8, 9, 11, 12, 13 ],  
          "visible": false  
        }
        ],
        "ordering": false,
        "ajax":{  
          url:"../laporan/fetch_user_open",  
        },  
        columns: [
        { "data": null },
        { "data": "jdRef" },
        { "data": "jobRef" },
        { "data": "jdNoDelivery" },
        { "data": null,
        render:function(data, type, row)
        {
          return moment(data['jdCreated']).format('DD-MMM-YYYY H:m:ss');
        }
        },
        { "data": "jobArea" },
        { "data": "plat" },
        { "data": "jobCourier" },
        { "data": "jdReceiver" },
        { "data": "customerAddress" },
        { "data": "customerName" },
        { "data": "type_of_goods" },
        { "data": "jdQty" },
        { "data": "jdNote" },
        { "data": "jdStatus" },
        { "data": null,
        render:function(data, type, row)
        {
          return '<button type="button" name="modal_image" id="'+ data['jdRef'] +'" class="modal_image btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
        }
      },
      { "data": null,
      render:function(data, type, row)
      {
        return '<a href="../laporan/hasil/'+ data['jdRef'] +'" target="_blank" type="button" name="modal_pdf" class="modal_pdf btn btn-default btn-xs">PDF</a>';
      }}
      ], 
    });
    dataopen.on( 'order.dt search.dt', function () {
      dataopen.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
      } );
    } ).draw();

    $('#table-filter').on('change', function(){
      dataopen.search(this.value).draw();   
    });

  $('#reportopen').on('apply.daterangepicker', function(ev, picker) {
    var start = picker.startDate;
    var end = picker.endDate;

      $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = start;
        var max = end;
        var startDate = new Date(data[4]);
        
        if (min == null && max == null) {
          return true;
        }
        if (min == null && startDate <= max) {
          return true;
        }
        if (max == null && startDate >= min) {
          return true;
        }
        if (startDate <= max && startDate >= min) {
          return true;
        }
        return false;
      }
    );
    dataopen.draw();
    $.fn.dataTable.ext.search.pop();
  });
});
// End Laporan

// Datatables Hapus Laporan
$(document).ready(function () {

  $(function() {
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('#reporthapus span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }


      $('#reporthapus').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
      }, cb);

      cb(start, end);

  });

    var datadelete = $('#hapus_laporan').DataTable({
        "oLanguage": {
            "sSearch": "Filter Data" //Will appear on search form
          },
       
          columnDefs:[
          {  
            "targets":[1, 2, 6, 8, 9, 11, 12, 13 ],  
          "visible": false  
        }
        ],
        "ordering": false,
        "ajax":{  
          url:"../laporan/fetch_user_delete",  
        },  
        columns: [
        { "data": null },
        { "data": "jdRef" },
        { "data": "jobRef" },
        { "data": "jdNoDelivery" },
        { "data": null,
        render:function(data, type, row)
        {
          return moment(data['jdCreated']).format('DD-MMM-YYYY H:m:ss');
        }
        },
        { "data": "jobArea" },
        { "data": "plat" },
        { "data": "jobCourier" },
        { "data": "jdReceiver" },
        { "data": "customerAddress" },
        { "data": "customerName" },
        { "data": "type_of_goods" },
        { "data": "jdQty" },
        { "data": "jdNote" },
        { "data": "jdStatus" },
        { "data": null,
        render:function(data, type, row)
        {
          return '<button type="button" name="modal_image" id="'+ data['jdRef'] +'" class="modal_image btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
        }
      },
      { "data": null,
      render:function(data, type, row)
      {
        return '<button type="button" name="hapus_laporan" kode="'+ data['jdRef'] +'" class="hapus_laporan"><i class="fas fa-trash-alt"></i></button>';
      }}
      ], 
    });
    datadelete.on( 'order.dt search.dt', function () {
      datadelete.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
      } );
    } ).draw();

    $('#table-filter').on('change', function(){
      datadelete.search(this.value).draw();   
    });

  $('#reporthapus').on('apply.daterangepicker', function(ev, picker) {
    var start = picker.startDate;
    var end = picker.endDate;

      $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        var min = start;
        var max = end;
        var startDate = new Date(data[4]);
        
        if (min == null && max == null) {
          return true;
        }
        if (min == null && startDate <= max) {
          return true;
        }
        if (max == null && startDate >= min) {
          return true;
        }
        if (startDate <= max && startDate >= min) {
          return true;
        }
        return false;
      }
    );
    datadelete.draw();
    $.fn.dataTable.ext.search.pop();
  });
});
// End Datatables

// Modal Detail
$(document).on('click', '.modal_image', function(){  
         var user_id = $(this).attr("id");  
           $.ajax({  
                url:"../laporan/fetch_single_user",  
=======
  );
  dataku.draw();
  $.fn.dataTable.ext.search.pop();
});
});



$(document).ready(function () {

$(function() {
  var start = moment().subtract(29, 'days');
  var end = moment();

  function cb(start, end) {
      $('#reportopen span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }


    $('#reportopen').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      }
    }, cb);

    cb(start, end);

  });

  var dataopen = $('#order_open').DataTable({
    "oLanguage": {
        "sSearch": "Filter Data" //Will appear on search form
      },
      dom: 'lrtip',
      dom: 'Bfrtip',
      buttons: [
      {
        extend: 'excel',
        text: 'EXPORT EXCEL',
        autoFilter: true,
        exportOptions: {
          columns:  [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 12]
        }               
      }
      ],
      columnDefs:[
      {  
       "targets":[ 1, 5, 7, 8, 10, 11, 12 ],  
       "visible": false  
     }
     ],
     "ordering": false,
     "ajax":{  
      url:"/WOI/laporan/fetch_user_open",  
    },  
    columns: [
    { "data": "jdRef" },
    { "data": "jobRef" },
    { "data": "jdNoDelivery" },
    { "data": null,
    render:function(data, type, row)
    {
      return moment(data['jdCreated']).format('DD-MMM-YYYY H:m:ss');
    }
    },
    { "data": "jobArea" },
    { "data": "jobNopol" },
    { "data": "jobCourier" },
    { "data": "jdReceiver" },
    { "data": "customerAddress" },
    { "data": "customerName" },
    { "data": "jdGoodsType" },
    { "data": "jdQty" },
    { "data": "jdNote" },
    { "data": "jdStatus" },
    { "data": null,
    render:function(data, type, row)
    {
      return '<button type="button" name="modal_image" id="'+ data['jdRef'] +'" class="modal_image btn btn-warning btn-xs"><i class="md-pageview">Detail</i></button>';
    }
  },
  { "data": null,
  render:function(data, type, row)
  {
    return '<a href="laporan/hasil/'+ data['jdRef'] +'" target="_blank" type="button" name="modal_pdf" class="modal_pdf btn btn-default btn-xs">PDF</a>';
  }}
  ],  
});
  $('#table-filter').on('change', function(){
    dataopen.search(this.value).draw();   
  });

  $('#reportopen').on('apply.daterangepicker', function(ev, picker) {
   var start = picker.startDate;
   var end = picker.endDate;

    $.fn.dataTable.ext.search.push(
    function(settings, data, dataIndex) {
      var min = start;
      var max = end;
      var startDate = new Date(data[3]);
      
      if (min == null && max == null) {
        return true;
      }
      if (min == null && startDate <= max) {
        return true;
      }
      if (max == null && startDate >= min) {
        return true;
      }
      if (startDate <= max && startDate >= min) {
        return true;
      }
      return false;
    }
  );
  dataopen.draw();
  $.fn.dataTable.ext.search.pop();
});
});


      $(document).on('click', '.modal_image', function(){  
         var user_id = $(this).attr("id");  
           $.ajax({  
                url:"/WOI/laporan/fetch_single_user",  
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3
                method:"POST",
                data:{user_id:user_id},  
                dataType:"json",  
                success:function(data)  
                {  
                     $('#tesModal').modal('show');  
                     $('#jobRef').text(data.jobRef);
<<<<<<< HEAD
                     $('#jdNoDelivery').text(data.jdNoDelivery);
=======
                     $('#customerCode').text(data.customerCode);
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3
                     $('#jdConsignee').text(data.jobCourier);
                     $('#jdReceiver').text(data.jdReceiver);
                     $('#jdReceiverTitle').text(data.jdReceiverTitle);
                     $('#customerAddress').text(data.customerAddress);
<<<<<<< HEAD
                     $('#jdGoodsType').text(data.typeId);
=======
                     $('#jdGoodsType').text(data.jdGoodsType);
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3
                     $('#jdQty').text(data.jdQty);
                     $('#jdNote').text(data.jdNote);
                     $('#jdStatus').text(data.jdStatus);
                     $('#jdimg1').html(data.Image1);
                     $('#jdimg2').html(data.Image2);
                     $('#jdimg3').html(data.Image3);
                     $('#jdimg4').html(data.Image4);
                     $('.modal-title #jdRef').text(user_id);
                     $('.modal-title #jdUpdated').text(data.jdUpdated);  
                } 
      });

<<<<<<< HEAD
});
// End Modal

  function toggleSidebar(){
    document.getElementById ("left-sidebar").classList.toggle('active');
  }
  function parseDateValue(rawDate) {

    var d = moment(rawDate, "DD-MMM-YYYY").format('DD-MM-YYYY');
    var dateArray = d.split("-");
    var parsedDate = dateArray[2] + dateArray[1] + dateArray[0];
    return parsedDate;
  }

  $(".reportrange").on('apply.daterangepicker', function (ev, picker) {
=======
      });
      
         
      

        function toggleSidebar(){
        document.getElementById ("left-sidebar").classList.toggle('active');
        }
 function parseDateValue(rawDate) {

        var d = moment(rawDate, "DD-MMM-YYYY").format('DD-MM-YYYY');
        var dateArray = d.split("-");
        var parsedDate = dateArray[2] + dateArray[1] + dateArray[0];
        return parsedDate;
    }

$(".reportrange").on('apply.daterangepicker', function (ev, picker) {
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3

        ev.preventDefault();



        //if blank date option was selected
        if ((picker.startDate.format('DD-MMM-YYYY') == "01-Jan-0001") && (picker.endDate.format('DD-MMM-YYYY')) == "01-Jan-0001") {
            $(this).val('Blank');


            val = "^$";

            dataTable.column(dataIdx)
               .search(val, true, false, true)
               .draw();

        }
        else {
            //set field value
            $(this).val(picker.startDate.format('DD-MMM-YYYY') + ' to ' + picker.endDate.format('DD-MMM-YYYY'));



            //run date filter
            startDate = picker.startDate.format('DD-MMM-YYYY');
            endDate = picker.endDate.format('DD-MMM-YYYY');

            var dateStart = parseDateValue(startDate);
            var dateEnd = parseDateValue(endDate);

            var filteredData = dataTable
                    .column(dataIdx)
                    .data()
                    .filter(function (value, index) {

                        var evalDate = value === "" ? 0 : parseDateValue(value);
                        if ((isNaN(dateStart) && isNaN(dateEnd)) || (evalDate >= dateStart && evalDate <= dateEnd)) {

                            return true;
                        }
                        return false;
                    });


            var val = "";
            for (var count = 0; count < filteredData.length; count++) {

                val += filteredData[count] + "|";
            }

            val = val.slice(0, -1);


            dataTable.column(dataIdx)
                  .search(val ? "^" + val + "$" : "^" + "-" + "$", true, false, true)
                  .draw();
        }


       

<<<<<<< HEAD
  });

  $(".reportrange").on('cancel.daterangepicker', function (ev, picker) {
=======
    });


    $(".daterange").on('cancel.daterangepicker', function (ev, picker) {
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3
        ev.preventDefault();
        $(this).val('');
        dataTable.column(dataIdx)
              .search("")
              .draw();
<<<<<<< HEAD
  });


$(document).on('click', '.hapus_laporan', function(){  
    var kode = $(this).attr("kode");
    $("#idhapus").val(kode);
    $("#myModal").modal("show");

    $("#konfirmasi").click(function() {
        var kode = $("#idhapus").val();

        $.ajax({
            url: "hapuslaporan/hapus",
            type: "POST",
            data: {kode:kode},
            cache: false,
            success: function(html) {
               exit;
            }
        })
    })
}); 
=======
    });




>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3

 