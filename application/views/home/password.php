<form class="form-horizontal" action="" method="post" style="margin-top: 50px;">
<?php echo $message;?>
	<div class="form-group">
		<label class="col-md-8 col-lg-6 control-label">Password Lama</label>
		<div class="col-md-8 col-lg-6">
			<input type="password" name="lama" class="form-control">
		</div>
		<?php echo form_error('lama');?>
	</div>

	<div class="form-group">
		<label class="col-md-8 col-lg-6 control-label">Password Baru</label>
		<div class="col-md-8 col-lg-6">
			<input type="password" name="baru" class="form-control">
		</div>
		<?php echo form_error('baru');?>
	</div>

	<div class="form-group">
		<label class="col-md-8 col-lg-6 control-label">Konfirmasi Password</label>
		<div class="col-md-8 col-lg-6">
			<input type="password" name="konfirmasi" class="form-control">
		</div>
		<?php echo form_error('konfirmasi');?>
	</div>

	<div class="form-group">
		<label class="col-md-8 col-lg-6 control-label"></label>
		<div class="col-md-8 col-lg-6">
			<button class="btn btn-primary"><i class="glyphicon glyphicon-saved"></i> Update</button>
		</div>
	</div>
</form>