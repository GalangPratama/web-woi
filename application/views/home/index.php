<div class="section-header">
            <h1><?php echo $title; ?></h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item active"><a href="<?php echo site_url('home'); ?>">Dashboard</a></div>
             
            </div>
          </div>
          <div class="row">
              <div class="col-12">
                <div class="card">
                
                  <div class="card-body">
              
                      <div class="row justify-content-md-center" id="row-card">
                        <div class="col-3 card card-primary" id="card-order">
                         <a href="<?php echo site_url('laporan'); ?>">
                          <div class="card-header" id="card-home">
                            <p>ORDER DELIVERED</p>
                          </div>
                          <div class="card-body">
                            <p style="text-align: center; font-size: 40px; font-weight: bold;"><?php echo $deliv; ?></p>
                          </div>
                        </a>
                      </div>
                        
                        <div class="col-3 card card-danger" id="card-order">
                          <a href="<?php echo site_url('laporan'); ?>">
                          <div class="card-header" id="card-home">
                            <p>ORDER RETURN</p>
                          </div>
                          <div class="card-body">
                            <p style="text-align: center; font-size: 40px; font-weight: bold;"><?php echo $return; ?></p>
                          </div>
                          </a>
                        </div>
                        <div class="col-3 card card-warning" id="card-order">
                          <a href="<?php echo site_url('laporan/open'); ?>">
                          <div class="card-header" id="card-home">
                            <p>ORDER OPEN / PENDING</p>
                          </div>
                          <div class="card-body">
                            <p style="text-align: center; font-size: 40px; font-weight: bold;"><?php echo $open; ?></p>
                          </div>
                          </a>
                        </div>
                      </div>                      
      
                  </div>

                  <div class="row" id="chart_home">
                    <div class="col-12">
                      <div id="chart_pie"></div>
                    </div>
                  </div>

                  <div class="row" id="chart_home">
                    <div class="col-12">
                      <div id="chart_line"></div>
                    </div>
                  </div>

                  </div>
                </div>
              </div>
            </div>