<div class="section-header">
    <h1><?php echo $title; ?></h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?php echo site_url('home'); ?>">Dashboard</a></div>
        <div class="breadcrumb-item">Laporan</div>
    </div>
</div>
<div class="box-header">
    <form class="form-horizontal" action="" method="post">
        <div class="row mt-2">

            <div class="ftr-date col-4">
                <h5> Filter Tanggal : </h5>
                <div id="reporthapus"
                    style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down"></i>
                </div>
            </div>

            <div class="ftr-area col-4">
                <h5> Filter Area : </h5>
                <select id="table-filter"
                    style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
                    <option value="">All</option>
                    <option value="Palembang">Palembang</option>
                    <option value="Medan">Medan</option>
                </select>
            </div>

        </div>
</div>
</form>
</div>

<table id="hapus_laporan" class="table table-striped" style="width: 100%; text-align: center;">
    <thead>
        <tr>
            <th width="10%" style="text-align: center; color: #000000">No</th>
            <th width="10%" style="text-align: center; color: #000000">ID Kurir</th>
            <th width="10%" style="text-align: center; color: #000000">ID JOB</th>
            <th width="10%" style="text-align: center; color: #000000">Surat Jalan</th>
            <th width="10%" style="text-align: center; color: #000000">Tanggal</th>
            <th width="10%" style="text-align: center; color: #000000">Area</th>
            <th width="10%" style="text-align: center; color: #000000">Plat Nomer</th>
            <th width="10%" style="text-align: center; color: #000000">Nama Kurir</th>
            <th width="10%" style="text-align: center; color: #000000">Penerima</th>
            <th width="20%" style="text-align: center; color: #000000">Alamat</th>
            <th width="20%" style="text-align: center; color: #000000">Customer</th>
            <th width="20%" style="text-align: center; color: #000000">Item</th>
            <th width="20%" style="text-align: center; color: #000000">Quantity</th>
            <th width="10%" style="text-align: center; color: #000000">Note</th>
            <th width="10%" style="text-align: center; color: #000000">Status</th>
            <th width="10%" style="text-align: center; color: #000000">Job Detail</th>
            <th width="10%" style="text-align: center; color: #000000">Hapus Laporan</th>
        </tr>
    </thead>
</table>