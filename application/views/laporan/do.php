<?php foreach($job as $job):?>	
	<h4 style="text-align:center;">
		DELIVERED STATUS
	</h4>

	<hr>

	<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
        	<h5><b>SCHEDULED</b> &nbsp <b>:</b> <?php echo $job->jdCreated;?></h5>
        	<h5><b>SERIAL no.</b> &nbsp &nbsp <b>:</b> <?php echo $job->jobRef;?></h5>
        	<h5><b>SO Number</b> &nbsp &nbsp <b>:</b> <?php echo $job->jdRef;?></h5>
        	<h5><b>DELIVER TO</b> &nbsp <b>:</b> <?php echo $job->jdReceiver;?></h5>
        	<h5><b>Address</b> &nbsp &nbsp &nbsp &nbsp &nbsp<b>:</b> <?php echo $job->jdAddress;?></h5>
        </div>
         <div class="col-sm-4 invoice-col">
        	
        </div>
        <div class="col-sm-4 invoice-col">
        	<h5><b>STATUS</b> &nbsp &nbsp  &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <b>:</b> <?php echo $job->jdStatus;?></h5>
        	<h5><b>PAYMENT TYPE</b> &nbsp &nbsp &nbsp &nbsp &nbsp	<b>:</b> - </h5>
        	<h5><b>PAYMENT AMOUNT</b> &nbsp &nbsp <b>:</b> - </h5>
        	<h5><b>DISTANCE</b> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <b>:</b> <?php echo $job->jdDistance;?> km</h5>
        	<h5><b>TIME</b> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <b>:</b> <?php echo $job->jdHourArrived;?> </h5>
        </div>
    </div>
	
    <hr>

	<table class="table table-bordered">
		<thead style="background:lightgray;">
			<tr class="text-center">
				<td>TRACKING ID</td>
				<td>Description</td>
				<td>QTY</td>
				<td>Reject Reason</td>
			</tr>
		</thead>
			<tr class="text-center">
				<td><?php echo $job->jobRef;?></td>
				<td> - </td>
				<td><?php echo $job->jdQty;?></td>
				<td> - </td>
			</tr>
	</table>
    

	<div class="row invoice-info">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 invoice-col">
        	<h5><b>NOTE</b> &nbsp : <?php ?></h5>
        	<br>

        	<h5><b>LAST KNOWN APPROXIMATE LOCATION AT TIME OF SUBMISSION</b> &nbsp &nbsp	: </h5>
        	<br>
        	<h5><b>Address</b> &nbsp &nbsp <b>:</b> <?php echo $job->jdAddress;?></h5>
        	<h5><b>Date</b> &nbsp &nbsp &nbsp &nbsp &nbsp <b>:</b> <?php echo $job->jdCreated;?></h5>
        	<h5><b>Recepient</b> &nbsp<b>:</b> <?php echo $job->jdReceiver;?></h5>
        	<h5><b>Relation</b>&nbsp &nbsp &nbsp<b>:</b> - </h5>

        	<br>

        	<h5><b>PROVE OF DELIVERY :</b>  </h5>
        	<br>
        	<h5><b>Submmited By :</b>  </h5>
        	<br>
        	<h5>Signature and Photo : </h5>
        </div>
        <div class="col-xs-12 text-center">
        	<img width="400" src="data:image/jpeg;base64, <?php echo $job->apSignature; ?>"/>
        	<img width="300" src="data:image/jpeg;base64, <?php echo $job->apPhoto; ?>"/>
        </div>
        	
    </div>

	<br>
<?php endforeach;?>		

<div class="well">
	<button class="btn btn-primary" onClick="return print();">
		<i class="glyphicon glyphicon-print"></i> Cetak
	</button>
	<a href="<?php echo site_url('public/laporan');?>" class="btn btn-default">Kembali</a>
</div>

<script>
	function print(){
		window.open ("<?php echo site_url('public/laporan/hasil/' .$id);?>", 
			"mywindow","status=1,toolbar=1,width=1020,height=800");
	}
</script>