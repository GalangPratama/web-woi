<!--  -->
<style type="text/css">
  table,th,td{
    border-collapse: collapse;
    padding: 15px;
    margin: 10px;
    color: #000;
  }
</style>

<div style="text-align: center;">
  <span style="margin-left: 20px;font-size: 20px;" border="1" ><b>Laporan Kurier</b></span>
</div>
<br>
<table border="1">
      <thead>
        <tr>
          <th>No</th>
          <th>ID Kurir</th>
          <th>ID Job</th>
          <th>Tanggal</th>
          <th>Nama Kurir</th>
          <th>Penerima</th>
          <th>Alamat</th>
          <th>Keteranga</th>
          <th>Distance</th>
          <th>Waktu</th>
          <th>Status</th>
        </tr>
      </thead>
      <?php
       if( ! empty($semua)){
        $no = 1;  
         foreach ($semua as $data) {
            ?>
              <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $data->jdRef; ?></td>
                <td><?php echo $data->jobRef; ?></td>
                <td><?php echo date('d-m-Y', strtotime($data->jdCreated)); ?></td>
                <td><?php echo $data->jdConsignee; ?></td>
                <td><?php echo $data->jdReceiver; ?></td>
                <td width="300" style="text-align: center;"><?php echo $data->jdAddress; ?></td>
                <td><?php echo $data->jdNote; ?></td>
                <td><?php echo $data->jdDistance; ?> km</td>
                <td><?php echo $data->jdHourArrived; ?></td>
                <td><?php echo $data->jdStatus; ?></td>
              </tr>
            <?php
          }
        }
      ?>
</table>