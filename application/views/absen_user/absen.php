<div class="section-header">
    <h1><?php echo $title; ?></h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?php echo site_url('home'); ?>">Dashboard</a></div>
        <div class="breadcrumb-item">Absen</div>
    </div>
</div>
<div class="box-header">
    <form class="form-horizontal" action="" method="post">
        <div class="row mt-2">

            <!-- <div class="ftr-date col-4">
          <h5> Filter Date : </h5>
             <div id="reportabsen" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
              <i class="fa fa-calendar"></i>&nbsp;
              <span></span> <i class="fa fa-caret-down"></i>
            </div>
        </div> -->

            <!-- <div class="ftr-area col-4">
          <h5> Filter Area : </h5>
          <select id="table-filter" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
            <option value="">All</option>
            <option>Palembang</option>
            <option>Medan</option>
          </select>
        </div> -->

        </div>
</div>
</form>
</div>
<div class="pull-right" id="pull_right">

</div>


<table id="absen" class="table table-striped" style="width: 100%; text-align: center;">
    <thead>
        <tr>
            <th width="10%" style="text-align: center;">No.</th>
            <th width="10%" style="text-align: center;">Surat jalan</th>
            <th width="10%" style="text-align: center;">Tanggal</th>
            <th width="10%" style="text-align: center;">Nama</th>
            <th width="10%" style="text-align: center;">Jam Masuk</th>
            <th width="10%" style="text-align: center;">Jam Pulang</th>
        </tr>
    </thead>
</table>