<div class="section-header">
    <h1><?php echo $title; ?></h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?php echo site_url('home'); ?>">Dashboard</a></div>
        <div class="breadcrumb-item active"><a href="<?php echo site_url('user'); ?>">User</a></div>
        <div class="breadcrumb-item">Edit User</div>
    </div>
</div>
<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
    <?php echo $message;?>
    <div class="form-group">
        <label class="col-md-4 control-label">Username</label>
        <div class="col-md-4">
            <input type="text" readonly="readonly" value="<?php echo $user['username'];?>" class="form-control"
                name="user">
        </div>
        <?php echo form_error('user');?>
    </div>


    <div class="form-group">
        <label class="col-md-4 control-label">Nama Lengkap</label>
        <div class="col-md-4">
            <input type="text" name="nama" value="<?php echo $user['nama_lengkap'];?>" class="form-control">
        </div>
        <?php echo form_error('nama');?>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">Level</label>
        <div class="col-md-4">
            <select name="level" class="form-control">
                <option value="<?php echo $user['level'];?>"><?php  if ($user['level'] == 1) {
                echo "Super Admin";
                } else if($user['level'] == 2) {
                    echo "Admin";
                }else{
                    echo "Vendor";
                } ?></option>
                <option value="1">Super Admin</option>
                <option value="2">Admin</option>
                <option value="3">Vendor</option>

            </select>
        </div>
        <?php echo form_error('level');?>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label">Foto</label>
        <div class="col-lg-4">
            <img src="<?php echo base_url('assets/img/profil/'.$user['foto']);?>" width="140px" height="140px"
                class="img img-rounded">
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label"></label>
        <div class="col-lg-4">
            <input type="file" name="gambar">
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-2 control-label"></label>
        <div class="col-lg-4">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-saved"></i> Simpan</button>
            <a href="<?php echo site_url('user');?>" class="btn btn-default">Kembali</a>
        </div>
    </div>
</form>