<div class="section-header">
    <h1><?php echo $title; ?></h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="<?php echo site_url('home'); ?>">Dashboard</a></div>
	  <div class="breadcrumb-item active"><a href="<?php echo site_url('user'); ?>">User</a></div>
      <div class="breadcrumb-item">Detail User</div>
    </div>
</div>
<table class="table">
	<tr>
		<td>Nama Lengkap</td>
		<td> : <?php echo $user['nama_lengkap'];?></td>
	</tr>

	<tr>
		<td>Username</td>
		<td> : <?php echo $user['username'];?></td>
	</tr>

	<tr>
		<td>Level</td>
		<td> : <?php echo $user['level'];?></td>
	</tr>
</table>

<hr>
<div class="well">
	<a href="<?php echo site_url('user');?>" class="btn btn-primary">Kembali</a>
</div>