<div class="section-header">
    <h1><?php echo $title; ?></h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?php echo site_url('home'); ?>">Dashboard</a></div>
        <div class="breadcrumb-item active"><a href="<?php echo site_url('user'); ?>">User</a></div>
        <div class="breadcrumb-item">Tambah User</div>
    </div>
</div>

<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
    <div class="form-group">
        <label class="col-lg-12 control-label">Nama Lengkap</label>
        <div class="col-md-8 col-lg-6">
            <input type="text" class="form-control" name="nama">
        </div>
        <?php echo form_error('nama');?>
    </div>

    <div class="form-group">
        <label class="col-lg-12 control-label">Username</label>
        <div class="col-md-8 col-lg-6">
            <input type="text" class="form-control" name="user">
        </div>
        <?php echo form_error('user');?>
    </div>

    <div class="form-group">
        <label class="col-lg-12 control-label">Password</label>
        <div class="col-md-8 col-lg-6">
            <input type="password" class="form-control" name="password">
        </div>
        <?php echo form_error('password');?>
    </div>

    <div class="form-group">
        <label class="col-lg-12 control-label">Level</label>
        <div class="col-md-8 col-lg-6">
            <select name="level" class="form-control">
                <option></option>
                <option value="1">Super Admin</option>
                <option value="2">Admin</option>
                <option value="3">Vendor</option>
            </select>
        </div>
        <?php echo form_error('level');?>
    </div>

    <div class="form-group">
        <label class="col-lg-12 control-label">Foto</label>
        <div class="col-md-8 col-lg-6">
            <input type="file" name="gambar">
        </div>
    </div>

    <div class="form-group">
        <label class="col-lg-12 control-label"></label>
        <div class="col-md-8 col-lg-6">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-saved"></i> Simpan</button>
            <a href="<?php echo site_url('user');?>" class="btn btn-default">Kembali</a>
        </div>
    </div>
</form>