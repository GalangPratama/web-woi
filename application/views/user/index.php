<div class="section-header">
    <h1><?php echo $title; ?></h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="<?php echo site_url('home'); ?>">Dashboard</a></div>
        <div class="breadcrumb-item">User</div>
    </div>
</div>

<div class="row">
    <!-- <div class="col-md-8">
		<form class="form-horizontal" action="<?php echo site_url('user/filter');?>" method="post">
			<div class="form-group">
				<label class="col-lg-4 control-label">Username / Nama User</label>
				<div class="col-lg-5">
					<input type="text" class="form-control">
				</div>
				<div class="col-lg-3">
					<button class="btn btn-default"><i class="glyphicon glyphicon-search"></i> Cari</button>
				</div>
			</div>
		</form>
	</div> -->


    <div class="col-md-4">
        <div class="pull pull-right">
            <a href="<?php echo site_url('user/tambah');?>" class="btn btn-primary"><i
                    class="glyphicon glyphicon-plus"></i> Tambah User</a>
        </div>
    </div>
</div>

<table class="table table-striped">
    <thead>
        <tr style="text-align: center">
            <th>No</th>
            <th>Nama Lengkap</th>
            <th>Username</th>
            <th>Level</th>
            <th>Foto</th>
            <th colspan="2">Action</th>
        </tr>
    </thead>
    <?php $no=0; foreach($user as $user): $no++;?>
    <tr style="text-align: center">
        <td><?php echo $no;?></td>
        <td><?php echo $user->nama_lengkap;?></td>
        <td><?php echo $user->username;?></td>
        <td><?php  if ($user->level == 1) {
           echo "Super Admin";
        } else if($user->level == 2) {
            echo "Admin";
        }else{
            echo "Vendor";
        } ?></td>
        <td><?php echo $user->foto;?></td>
        <td><a href="<?php echo site_url('user/edit/'.$user->username);?>"><i class="fas fa-edit"></i></a></td>
        <td><a href="#" title="Hapus User" class="hapus" kode="<?php echo $user->username;?>"><i
                    class="fas fa-trash-alt"></i></a></td>
    </tr>
    <?php endforeach;?>
</table>

<script>

</script>