 <?php $this->load->view('template/css'); ?>
 <body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
     <?php $this->load->view('template/header'); ?>
      <?php $this->load->view('template/sidebar'); ?>
            <!-- Main content -->
          <div class="main-content">
            <section class="section">
              <div class="content">
                <?php echo $content;?>
              </div>
            </section>
          </div>
        <?php $this->load->view('template/footer'); ?>
    </div>
  </div>

    <!-- Modal Delete -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Konfirmasi</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" name="idhapus" id="idhapus">
            <input type="hidden" name="idhapus2" id="idhapus2">
            <p>Apakah anda yakin ingin menghapus data ini?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="konfirmasi">Hapus</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End Modal Delete -->

    <!-- Modal Image Laporan -->
    <div class="modal fade" id="tesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> 
              <div class="left-mdl">
              ID Number : <span id="jdRef"></span>
              </div>
              <div class="right-mdl">
              Tanggal &nbsp&nbsp&nbsp&nbsp : <span id="jdUpdated"></span>
              </div>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <table id="mdl_gambar" class="table table-striped">
            <thead style="width: 100%;">
              <tr>
                <th class="text-center">ID JOB</th>
                <th class="text-center">Surat Jalan</th>
                <th class="text-center">Nama Kurir</th>
                <th class="text-center">Penerima</th>
                <th class="text-center">Position</th>
                <th class="text-center">Alamat</th>
                <th class="text-center">Produk</th>
                <th class="text-center">Jumlah</th>
                <th class="text-center">Keterangan</th>
                <th class="text-center">Status</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="text-center" id="jobRef"> </td>
<<<<<<< HEAD
                <td class="text-center" id="jdNoDelivery"> </td>
=======
                <td class="text-center" id="customerCode"> </td>
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3
                <td class="text-center" id="jdConsignee"> </td>
                <td class="text-center" id="jdReceiver"> </td>
                <td class="text-center" id="jdReceiverTitle"> </td>
                <td class="text-center" id="customerAddress"> </td>
                <td class="text-center" id="jdGoodsType"> </td>
                <td class="text-center" id="jdQty"> </td>
                <td class="text-center" id="jdNote"> </td>
                <td class="text-center" id="jdStatus"> </td>
              </tr>
            </tbody>
          </table>
          <div class="Image">
            <div class="judul-img">
             <h4>IMAGE</h4>
           </div>
           <div class="img">
            <span id="jdimg1"> </span>
            <span id="jdimg2"> </span>
            <span id="jdimg3"> </span>
            <span id="jdimg4"> </span>
          </div>
        </div>
      </div>
      <div class="modal-footer">   
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
      </div>  
    </div>
  </div>
</div>
  <!-- End Modal Image Laporan -->

  <!-- Modal Add Vehicle -->
  <div class="modal fade" id="add_vehicle" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">Tambah Kendaraan</h5>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url().'vehicle/tambah'?>">
          <div class="modal-body">

           <div class="form-group">
            <label>Kendaraan</label>
            <select class="form-control selectric" name="type" required>
              <option>--Pilih Kendaraan--</option>
              <option value="motor">Motor</option>
              <option value="mobil">Mobil</option>
            </select>
            <?php echo form_error('type');?>
          </div>

          <div class="form-group">
            <label>Plat Nomer</label>
            <div class="input-group">
              <input type="text" name="plat" class="form-control" required>
            </div>
            <?php echo form_error('plat');?>
          </div>

          <div class="form-group">
            <div class="control-label">Status Aktif</div>
            <div class="custom-switches-stacked mt-2">
              <label class="custom-switch">
                <input type="radio" name="status" value="Y" class="custom-switch-input" checked>
                <span class="custom-switch-indicator"></span>
                <span class="custom-switch-description">Aktif</span>
              </label>
              <label class="custom-switch">
                <input type="radio" name="status" value="N" class="custom-switch-input">
                <span class="custom-switch-indicator"></span>
                <span class="custom-switch-description">Non Aktif</span>
              </label>
            </div>
            <?php echo form_error('status');?>
          </div>

        </div>

        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
          <button class="btn btn-info">Save</button>
        </div>
      </form>
    </div>
  </div>
  </div>
  <!-- End Modal Add Vehicle -->

    <!-- Modal Add Vehicle -->
  <div class="modal fade" id="add_produk" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="myModalLabel">Tambah Produk</h5>
        </div>
        <form class="form-horizontal" method="post" action="<?php echo base_url().'produk/tambah'?>">
          <div class="modal-body">

          <div class="form-group">
            <label>Nama Produk</label>
            <div class="input-group">
              <input type="text" name="nama_produk" class="form-control" required>
            </div>
            <?php echo form_error('nama_produk');?>
          </div>

          <div class="form-group">
            <div class="control-label">Status Aktif</div>
            <div class="custom-switches-stacked mt-2">
              <label class="custom-switch">
                <input type="radio" name="status" value="Y" class="custom-switch-input" checked>
                <span class="custom-switch-indicator"></span>
                <span class="custom-switch-description">Aktif</span>
              </label>
              <label class="custom-switch">
                <input type="radio" name="status" value="N" class="custom-switch-input">
                <span class="custom-switch-indicator"></span>
                <span class="custom-switch-description">Non Aktif</span>
              </label>
            </div>
            <?php echo form_error('status');?>
          </div>

        </div>

        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
          <button class="btn btn-info">Save</button>
        </div>
      </form>
    </div>
  </div>
  </div>
<<<<<<< HEAD
=======
  <!-- End Modal Add Vehicle -->
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3


    <?php $this->load->view('template/js'); ?>
</div>
</body>
</html>
