<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="<?php echo site_url('home'); ?>">Wicaksana O.I</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?php echo site_url('home'); ?>">W.O.I</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li <?= $this->uri->segment(1) == 'home' || $this->uri->segment(1) == '' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url('home'); ?>"><i class="fas fa-fire"></i><span>Home</span></a>
            </li>
            </li>
            <li
                class="dropdown <?= $this->uri->segment(2) == 'delivered' || $this->uri->segment(2) == 'open' ? 'active' : '' ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i>
                    <span>Laporan</span></a>
                <ul class="dropdown-menu">
                    <li <?= $this->uri->segment(2) == 'delivered' ? 'class="active"' : '' ?>><a class="nav-link"
                            href="<?php echo site_url('laporan/delivered'); ?>"><i class="fas fa-share-square"></i>
                            Order
                            Selesai</a></li>
                    <li <?= $this->uri->segment(2) == 'open' ? 'class="active"' : '' ?>><a class="nav-link"
                            href="<?php echo site_url('laporan/open'); ?>"><i class="fas fa-external-link-alt"></i>
                            Order Open</a></li>
                </ul>
            </li>
            <li <?= $this->uri->segment(1) == 'maps' ? 'class="active"' : '' ?>>
                <a class="nav-link" href="<?php echo site_url('maps'); ?>"><i class="fas fa-map-marked-alt"></i>
                    <span>Maps Kurir</span></a>
            </li>
            <li>
                <a class="nav-link" href="<?php echo site_url('home/logout'); ?>"><i class="fas fa-sign-out-alt"></i>
                    <span>Logout</span></a>
            </li>

    </aside>
</div>