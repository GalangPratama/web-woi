<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="<?php echo site_url('home'); ?>">Wicaksana O.I</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="<?php echo site_url('home'); ?>">W.O.I</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li <?= $this->uri->segment(1) == 'home' || $this->uri->segment(1) == '' ? 'class="active"' : '' ?>>
                <a href="<?php echo site_url('home'); ?>"><i class="fas fa-fire"></i><span>Home</span></a>
            </li>
            <li
                class="dropdown <?= $this->uri->segment(1) == 'vehicle' || $this->uri->segment(1) == 'produk' || $this->uri->segment(2) == 'hapus_laporan' || $this->uri->segment(1) == 'user' ? 'active' : '' ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-layer-group"></i>
                    <span>Master</span></a>
                <ul class="dropdown-menu">
                    <li <?= $this->uri->segment(1) == 'vehicle' ? 'class="active"' : '' ?>><a class="nav-link"
                            href="<?php echo site_url('vehicle'); ?>"><i class="fas fa-motorcycle"></i>Kendaraan</a>
                    </li>
                    <li <?= $this->uri->segment(1) == 'produk' ? 'class="active"' : '' ?>><a class="nav-link"
                            href="<?php echo site_url('produk'); ?>"><i class="fas fa-box-open"></i>Produk</a></li>
                    <li <?= $this->uri->segment(2) == 'hapus_laporan' ? 'class="active"' : '' ?>><a class="nav-link"
                            href="<?php echo site_url('laporan/hapus_laporan'); ?>"><i class="fas fa-eraser"></i>Hapus
                            Laporan</a></li>
                </ul>
            </li>
            <li
                class="dropdown <?= $this->uri->segment(2) == 'delivered' || $this->uri->segment(2) == 'open' ? 'active' : '' ?>">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-table"></i>
                    <span>Laporan</span></a>
                <ul class="dropdown-menu">
                    <li <?= $this->uri->segment(2) == 'delivered' ? 'class="active"' : '' ?>><a class="nav-link"
                            href="<?php echo site_url('laporan/delivered'); ?>"><i class="fas fa-share-square"></i>
                            Order
                            Selesai</a></li>
                    <li <?= $this->uri->segment(2) == 'open' ? 'class="active"' : '' ?>><a class="nav-link"
                            href="<?php echo site_url('laporan/open'); ?>"><i class="fas fa-external-link-alt"></i>
                            Order Open</a></li>
                </ul>
            </li>
            <li <?= $this->uri->segment(1) == 'absen' ? 'class="active"' : '' ?>>
                <a class="nav-link" href="<?php echo site_url('absen'); ?>"><i class="fas fa-clipboard-list"></i>
                    <span>Absen Kurier</span></a>
            </li>
            <li <?= $this->uri->segment(1) == 'maps' ? 'class="active"' : '' ?>>
                <a class="nav-link" href="<?php echo site_url('maps'); ?>"><i class="fas fa-map-marked-alt"></i>
                    <span>Maps
                        Kurir</span></a>
            </li>
            <li>
                <a class="nav-link" href="<?php echo site_url('home/logout'); ?>"><i class="fas fa-sign-out-alt"></i>
                    <span>Logout</span></a>
            </li>
        </ul>

    </aside>
</div>