<?php
class M_home extends CI_Model{
	private $table="tb_jobdetail";
	private $primary="jobRef";
	

	function get_deliv(){
		 $this->db->select("jdStatus");  
          $this->db->from('tb_jobdetail');
          $this->db->where('jdStatus = "DELIVERED"');  
          return $this->db->count_all_results();
	}
	function get_rtn(){
		 $this->db->select("jdStatus");  
          $this->db->from('tb_jobdetail');
          $this->db->where('jdStatus = "RETURN"');  
          return $this->db->count_all_results();
	}
	function get_open(){
		 $this->db->select("jdStatus");  
          $this->db->from('tb_jobdetail');
          $this->db->where('jdStatus = "OPEN"');  
          return $this->db->count_all_results();
	}

	function get_tgl(){
		$hasil = $this->db->query("SELECT DATE_FORMAT(jdCreated,'%d %b %Y') Tanggal,
     COUNT(IF(jdStatus='DELIVERED',1, NULL)) 'DELIVERED',
     COUNT(IF(jdStatus='RETURN',1, NULL)) 'RETURN',
     COUNT(IF(jdStatus='OPEN',1, NULL)) 'OPEN'
     FROM tb_jobdetail
     WHERE jdCreated
     GROUP BY DATE_FORMAT(jdCreated,'%d %b %Y')");
    $query= $hasil->result();
    return $query;
	}

  function get_line_deliv(){
    $hasil = $this->db->query("SELECT COUNT(IF(jdStatus='DELIVERED',1, NULL)) 'DELIVERED'
      FROM tb_jobdetail
      WHERE jdCreated
      GROUP BY DATE_FORMAT(jdCreated,'%d %b %Y')");
    $result = $hasil->result_array();
        return $result;
  }

  function get_line_rtn(){
    $hasil = $this->db->query("SELECT COUNT(IF(jdStatus='RETURN',1, NULL)) 'RETURN'
      FROM tb_jobdetail
      WHERE jdCreated
      GROUP BY DATE_FORMAT(jdCreated,'%d %b %Y')");
    $result = $hasil->result_array();
        return $result;
  }

  function get_line_open(){
    $hasil = $this->db->query("SELECT COUNT(IF(jdStatus='OPEN',1, NULL)) 'OPEN'
      FROM tb_jobdetail
      WHERE jdCreated
      GROUP BY DATE_FORMAT(jdCreated,'%d %b %Y')");
    $result = $hasil->result_array();
        return $result;
  }

	function get_chart(){
		$query=$this->db->query("select * from tb_jobdetail;");
		return $query;
	}
	

}

