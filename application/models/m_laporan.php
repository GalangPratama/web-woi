<?php
class M_laporan extends CI_Model{
	private $table="tb_jobdetail";
	private $primary="jobRef";

	function semua($table){
      	 $this->db->where('jdStatus <> "RETURN"');
        return $this->db->get($table)->result();
	}

	function open($table){
      	 $this->db->where('jdStatus <> "DELIVERED"');
        return $this->db->get($table)->result();
	}

	function jobdetail(){
		$this->db->select('*');
		$this->db->from('tb_jobdetail');
		$this->db->order_by('jdRef', 'ASC');
		$this->db->join('tb_job','tb_jobdetail.jobRef=tb_job.jobRef');
		return $this->db->get();
	}

	 function cekDo($id){
        $this->db->select('*');
        $this->db->from('tb_jobdetail');
        $this->db->where('tb_jobdetail.jdRef',$id);
        $this->db->join('tb_job','tb_jobdetail.jobRef=tb_job.jobRef');
<<<<<<< HEAD
        $this->db->join('tb_types_goods','tb_jobdetail.typeId=tb_types_goods.id', 'left'); 
        $this->db->join('tb_vehicle','tb_job.vehicleId=tb_vehicle.id', 'left'); 
        $this->db->join('tb_customers','tb_jobdetail.customerCode=tb_customers.customerCode', 'left');
=======
        $this->db->join('tb_customers','tb_jobdetail.customerCode=tb_customers.customerCode', 'left');
        $this->db->join('tb_pod_signatures','tb_jobdetail.jdRef=tb_pod_signatures.jdRef', 'left');
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3
        return $this->db->get();
    }
     function photo($id){
        $this->db->select('*');
        $this->db->from('tb_jobdetail');
        $this->db->where('tb_jobdetail.jdRef',$id);
        $this->db->join('tb_pod_photo','tb_jobdetail.jdRef=tb_pod_photo.jdRef', 'left');
<<<<<<< HEAD
        $this->db->join('tb_pod_signatures','tb_jobdetail.jdRef=tb_pod_signatures.jdRef', 'left');
=======
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3
        return $this->db->get();
    }

	function per_job($jobRef){
        $this->db->from('tb_jobdetail');
        $this->db->where('tb_jobdetail.jobRef',$jobRef);
        $this->db->join('tb_job','tb_jobdetail.jobRef=tb_job.jobRef');
        return $this->db->get();
    }

	function filter_laporan($start_date, $end_date){
		
		$this->db->select('*');
		$this->db->from('tb_jobdetail');
		$this->db->where('jdStatus <> "OPEN"');
		$this->db->where('jdCreated >=' ,$start_date);
		$this->db->where('jdCreated <=' ,$end_date);
		$this->db->join('tb_job','tb_jobdetail.jobRef=tb_job.jobRef');
		return $this->db->get()->result();
	}
	function head($id){
        $this->db->from('tb_jobdetail');
        $this->db->where('tb_jobdetail.jdRef',$id);
        $this->db->join('tb_job','tb_job.jobReff=tb_jobdetail.jdRef');
        return $this->db->get();
    }
}

