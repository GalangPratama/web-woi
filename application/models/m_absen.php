<?php
class M_absen extends CI_Model{
  var $table = "tb_job";  
  var $select_column = array("jobRef", "jobDate", "jobCourier", "jobCreated", "assetsCreated");  
  var $order_column = array("jobRef", "jobDate", "jobCourier", "jobCreated", "assetsCreated", null); 

	    function make_query()  
      {  
        $this->db->select("tb_job.jobDate, tb_jobdetail.jdNoDelivery, tb_job.jobCourier,
        MIN(assetsCreated) AS Masuk, CASE WHEN MAX(assetsCreated)<>MIN(assetsCreated) THEN MAX(tb_assets.assetsCreated) END AS Pulang", FALSE);
        $this->db->from('tb_job');
        $this->db->order_by('jobDate', 'DESC');
        $this->db->join('tb_jobdetail','tb_job.jobRef=tb_jobdetail.jobRef', 'left');
        $this->db->join('tb_assets','tb_job.jobRef=tb_assets.jobRef', 'left');
        $this->db->group_by('tb_assets.jobRef');
      }  
      
      function make_datatables(){  
        $this->make_query();
        $query = $this->db->get();  
        return $query->result();  
      }

      function make_assets(){
        $this->db->select('*');
        $this->db->from('tb_assets');
        $this->db->get()->result();
      }
}