<?php
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library(array('form_validation'));
		$this->load->model('m_user');
		$this->cekLogin();
	}
<<<<<<< HEAD
 

	function index(){
		$data['title']="Login";
		$data['message']="";
		$this->load->view('login/index',$data);
	}
 
	function auth(){
		$user=$this->input->post('userid');
		$pass=$this->input->post('password');
 
        $cek_auth=$this->m_user->login($user,$pass);
 
        if($cek_auth->num_rows() > 0){ //jika login sebagai Admin
                $data=$cek_auth->row_array();
                $this->session->set_userdata('is_login',TRUE);
                 if($data['level']=='1'){ //Akses IT
                    $this->session->set_userdata('akses','1');
                    $this->session->set_userdata('ses_id',$data['username']);
                    $this->session->set_userdata('ses_nama',$data['nama_lengkap']);
                    redirect('home');
 
                 }else if($data['level']=='2'){ //akses Admin
                    $this->session->set_userdata('akses','2');
                    $this->session->set_userdata('ses_id',$data['username']);
                    $this->session->set_userdata('ses_nama',$data['nama_lengkap']);
					redirect('home');
					
                 }else{ //akses Vendor
					$this->session->set_userdata('akses','3');
                    $this->session->set_userdata('ses_id',$data['username']);
                    $this->session->set_userdata('ses_nama',$data['nama_lengkap']);
                    redirect('home');
				 }
 
		}else{ // jika username dan password tidak ditemukan atau salah
			$data['title']="Login";
			$data['message']="<div class='alert alert-danger'>Username atau Password salah</div>";
			$this->load->view('login/index',$data);
        }
  
    }
=======

	function index(){
		$data['title']="Login";
		$this->form_validation->set_rules('userid','Username','required');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('level','Level','required');

		if($this->form_validation->run()==true){
			$user=$this->input->post('userid');
			$pass=md5($this->input->post('password'));
			$level=$this->input->post('level');

			$cek=$this->m_user->login($user,$pass,$level);

			if($cek->num_rows()>0){
				$this->session->set_userdata('is_login',true);
				$this->session->set_userdata('username',$user);
				$this->session->set_userdata('level',$level);

				redirect('home');
			}else{
				$data['message']="<div class='alert alert-danger'>Username atau Password salah</div>";
				$this->load->view('login/index',$data);
			}
		}else{
			$data['message']="";
			$this->load->view('login/index',$data);
		}
	}
>>>>>>> a37b4abfac64d11c9bd41f89f9226867e91cb1a3

	function cekLogin(){
		if($this->session->userdata('is_login')==true){
			redirect('home');
		}
	}
}