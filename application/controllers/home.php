<?php 
class home extends CI_Controller{
	var $folder   =   "maps";
	var $tables   =   "tb_jobdetail";
	var $pk       =   "jdRef";

	private $def_lat;
	private $def_lng;

	function __construct(){
		parent::__construct();
		$this->load->library(['template','pagination','form_validation']);
		$this->load->model(['m_home']);

		//---------------CSS-------------------
		
		// ---------------Jquery------------- 
		
		$this->template->add_includes('js', 'assets/highchart/highcharts.js');
		$this->template->add_includes('js', 'assets/highchart/highcharts-3d.js');
		$this->template->add_includes('js', 'assets/highchart/modules/exporting.js');
		$this->template->add_includes('js', 'assets/highchart/modules/export-data.js');
		$this->template->add_includes('js', 'assets/js/custom_chart.js');

		
		if($this->session->userdata('is_login')==false){
			redirect('login');
		}

	}

	function index(){
		$data['title']="Selamat Datang";
		$data['return'] = $this->m_home->get_rtn();
		$data['deliv'] = $this->m_home->get_deliv();
		$data['open'] = $this->m_home->get_open();
		$this->template->load('template', 'home/index', $data);
	}

	function get_pie(){
		
		$col1='DELIVERED';
		$col2='RETURN';
		$col3='OPEN';
		
		$cell1=$this->m_home->get_deliv();
		$cell2=$this->m_home->get_rtn();
		$cell3=$this->m_home->get_open();

		$row1 = array('name'=>$col1, 'y'=>$cell1);
		$row2 =array('name'=>$col2, 'y'=>$cell2);
		$row3  = array('name'=> $col3, 'y'=> $cell3);
		$data = array($row1, $row2, $row3);
		
		print_r(json_encode($data));
	}

	function get_line(){
		$data = $this->m_home->get_tgl();

		$category = array();
		$category['name'] = 'Tanggal';

		$series1 = array();
		$series1['name'] = 'DELIVERED';

		$series2 = array();
		$series2['name'] = 'RETURN';

		$series3 = array();
		$series3['name'] = 'OPEN/PENDING';

		foreach ($data as $row)
		{
			$category['data'][] = $row->Tanggal;
			$series1['data'][] = $row->DELIVERED;
			$series2['data'][] = $row->RETURN;
			$series3['data'][] = $row->OPEN;
		}

		$result = array();
		array_push($result,$category);
		array_push($result,$series1);
		array_push($result,$series2);
		array_push($result,$series3);


		print json_encode($result, JSON_NUMERIC_CHECK);

	}
	

	function setting(){
		$data['title']="Setting Identitas Perusahaan";
		$this->form_validation->set_rules('perusahaan','Nama Perusahaan','required');
		$this->form_validation->set_rules('pemilik','Nama Pemilik','required');
		$this->form_validation->set_rules('alamat','Alamat','required');

		if($this->form_validation->run()==true){

			$info=array(
				'nm_perusahaan'=>$this->input->post('perusahaan'),
				'pemilik'=>$this->input->post('pemilik'),
				'alamat'=>$this->input->post('alamat')
			);

			$cek=$this->m_identitas->identitas();
			if($cek->num_rows()>0){
				$this->m_identitas->update($info);
				$data['message']="<div class='alert alert-info'>Data Berhasil diupdate</div>";
			}else{
				$this->m_identitas->simpan($info);
				$data['message']="<div class='alert alert-info'>Data Berhasil disimpan</div>";
			}
		}else{
			$data['message']="";
		}
		$data['identitas']=$this->m_identitas->identitas()->row_array();
		$this->template->load('template','home/setting',$data);
	}

	function password(){
		$data['title']="Password";

		$this->form_validation->set_rules('lama','Password Lama','required');
		$this->form_validation->set_rules('baru','Password Baru','required');
		$this->form_validation->set_rules('konfirmasi','Konfirmasi Password','required|matches[baru]');

		if($this->form_validation->run()==true){
			$this->load->model('m_user');
			$username=$this->session->userdata('username');
			$lama=md5($this->input->post('lama'));

			$cek=$this->m_user->cekPassword($username,$lama);
			if($cek->num_rows()>0){
				$info=array(
					'password'=>md5($this->input->post('baru'))
				);
				$this->m_user->updatePassword($username,$info);
				$data['message']="<div class='alert alert-info'>Password Berhasil diubah</div>";
			}else{
				$data['message']="<div class='alert alert-danger'>Password Lama tidak sesuai</div>";
			}
		}else{
			$data['message']="";
		}
		$this->template->load('template','home/password',$data);
	}

	function profile(){
		$data['title']="User Profile";
		$data['profile']=$this->m_identitas->profile($this->session->userdata('username'))->row_array();
		$this->template->load('template','home/profile',$data);
	}

	function tidakada(){
		echo "Halaman tidak ditemukan";
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

	function cekLogin(){
		$islogin=$this->session->userdata('is_login');
		$username=$this->session->userdata('username');

		if($this->session->userdate('is_login')==false){
			redirect('login');
		}
	}
}