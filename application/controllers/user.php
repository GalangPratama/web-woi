<?php
class User extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library(array('template','form_validation','pagination','upload'));
		$this->load->model('m_user');

		if($this->session->userdata('is_login')==false){
			redirect('login');
		}

	}
 
	function index(){
		$data['title']="Data User";
		$data['user']=$this->m_user->semua()->result();
		$this->template->load('template', 'user/index', $data);
	}

	function tambah(){
		$data['title']="Tambah Data User";
		$this->validasi();

		if($this->form_validation->run()==true){
			$user=$this->input->post('user');
			$cek=$this->m_user->cek($user);
			if($cek->num_rows()>0){
				$data['message']="<div class='alert alert-danger'>Username sudah digunakan</div>";
				$this->template->load('template', 'user/tambah',$data);
			}else{
				//setting konfiguras upload image
	            $config['upload_path'] = './assets/img/';
	    	    $config['allowed_types'] = 'gif|jpg|png';
	    	    $config['max_size']	= '1000';
	    	    $config['max_width']  = '2000';
	    	    $config['max_height']  = '1024';
	                
	            $this->upload->initialize($config);
	            if(!$this->upload->do_upload('gambar')){
	                $gambar="";
	            }else{
	                $gambar=$this->upload->file_name;
	            }

				$info=array(
					'username'=>$this->input->post('user'),
					'nama_lengkap'=>$this->input->post('nama'),
					'password'=>md5($this->input->post('password')),
					'level'=>$this->input->post('level'),
					'foto'=>$gambar
				);
				$this->m_user->simpan($info);
				redirect('user/detail/'.$info['username']);
			}
		}else{
			$data['message']="";
			$this->template->load('template', 'user/tambah',$data);
		}
	}

	function edit($id){
		$data['title']="Edit Data User";
		$this->form_validation->set_rules('nama','Nama Lengkap','required');
		$this->form_validation->set_rules('user','Username','required|max_length[8]');
		$this->form_validation->set_rules('level','Level','required');

		if($this->form_validation->run()==true){
			//setting konfiguras upload image
	        $config['upload_path'] = './assets/img/profil/';
	    	$config['allowed_types'] = 'gif|jpg|png';
	    	$config['max_size']	= '1000';
	    	$config['max_width']  = '2000';
	    	$config['max_height']  = '1024';

	    	$user=$this->input->post('user');
	                
	        $this->upload->initialize($config);
	        if(!$this->upload->do_upload('gambar')){
	            $info=array(
					'nama_lengkap'=>$this->input->post('nama'),
					'level'=>$this->input->post('level')
				);
	        }else{
	        	$info=array(
					'nama_lengkap'=>$this->input->post('nama'),
					'level'=>$this->input->post('level'),
					'foto'=>$this->upload->file_name
				);
	        }
			
	        $this->m_user->update($id,$info);
	        $data['message']="<div class='alert alert-info'>Data Berhasil diupdate</div>";

		}else{
			print_r($this->upload->display_errors());
			$data['message']="";
		}
		$data['user']=$this->m_user->cek($id)->row_array();
		$this->template->load('template', 'user/edit', $data);
	}

	function hapus(){
		$kode=$this->input->post('kode');
		$detail=$this->m_user->cek($kode)->result();
		foreach($detail as $det):
		    unlink("assets/img/".$det->foto);
		endforeach;
		$this->m_user->hapus($kode);
	}

	function filter(){
		$data['title']="Filter Data";
		$kode=$this->input->post('cari');
		$cek=$this->m_user->cari($kode);

		if($cek->num_rows()>0){
			$data['user']=$cek->result();
			$this->template->load('template', 'user/filter',$data);
		}else{
			$this->template->load('template', 'user/tidakada',$data);
		}
	}

	function detail($id){
		$data['title']="Detail User";
		$data['user']=$this->m_user->cek($id)->row_array();
		$this->template->load('template', 'user/detail',$data);
	}

	function validasi(){
		$this->form_validation->set_rules('nama','Nama Lengkap','required');
		$this->form_validation->set_rules('user','Username','required|max_length[8]');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('level','Level','required');
	}

}