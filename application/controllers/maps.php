<?php
class Maps extends CI_Controller{
	var $folder   =   "maps";
	var $tables   =   "tb_jobdetail";
	var $pk       =   "jdRef";
	private $def_lat;
	private $def_lng;

	function __construct(){
		parent::__construct();
		$this->load->library(['template','pagination','form_validation','Googlemaps']);
		$this->load->model(['m_maps']);

		$this->template->add_includes('js', 'assets/js/googlemap.js');

		if($this->session->userdata('is_login')==false){
			redirect('login');
		}

		$this->def_lat=$this->config->item('default_lat');
		$this->def_lng=$this->config->item('default_lng');
	}

	function index(){
		$warehouse =$this->m_maps->getAll('tb_jobdetail')->result();
		//pagination
		//Set Lokasi pusat		
		$center='-6.248860, 106.953293';
		//Konfigurasi Gogle Map
		$cfg=array(
		'class'			=> 'map-canvas', //Class Div, disini bisa membuat map full size,dll
		'map_div_id'	=> 'map-canvas', //Definisi ID Default
		'center'		=> $center, //Set Lokasi pusat ke google map
		'zoom'			=> 'auto',	 // Besar map, katanya bagus yg 17 ini
		);
		

		$this->googlemaps->initialize($cfg);
		foreach ($warehouse as $row) {
			$marker = array();
			$marker['position'] = $row->jdLat.", ".$row->jdLong;
			$marker['infowindow_content'] = $row->jobCourier;
			$marker['icon'] = base_url().'/assets/img/marker_maps.png';
			$this->googlemaps->add_marker($marker);
			
		}
		
		//Buat HTML Map
		$data = array(
			'map'  => $this->googlemaps->create_map()
		);
		
			// Load the page
		$data['title']="Maps Kurir";
		$data['name']= $this->m_maps->getAll('tb_jobdetail')->result();
		$this->template->load('template','maps/index', $data);
	}

	function perkurir($id){
		$latlng = $this->m_maps->cekMap($id)->result();
		foreach ($latlng as $row ){
		$tes[] = $row->jdLat.", ".$row->jdLong;
		$center= $row->jdLat.", ".$row->jdLong;
		//Konfigurasi Gogle Map
		$cfg=array(
		'class'			=> 'map-kurir', //Class Div, disini bisa membuat map full size,dll
		'map_div_id'	=> 'map-kurir', //Definisi ID Default
		'center'		=> $center, //Set Lokasi pusat ke google map
		'zoom'			=> 'auto',	 // Besar map, katanya bagus yg 17 ini
		);
		
		$this->googlemaps->initialize($cfg);
	
		
			// $marker['position'] = $tes[0];
			// $marker['infowindow_content'] = $row->jobCourier;
			// $marker['icon'] = base_url().'/assets/img/kurier-ok.png';
			// $this->googlemaps->add_marker($marker);
			
			$marker = array();
			$marker['position'] = $row->jdLat.", ".$row->jdLong;
			$marker['infowindow_content'] = $row->jobCourier;
				if ($latlng[0]->jdLat.", ".$latlng[0]->jdLong) {
					$img_marker = base_url().'/assets/img/marker_maps.png';
				} else {
					$img_marker = base_url().'/assets/img/kurier.png';
				}
				
			$marker['icon'] = $img_marker;
			$this->googlemaps->add_marker($marker);

			$polyline['points'] = $tes;	
			$this->googlemaps->add_polyline($polyline);	
			}
		//Buat HTML Map
		$data = array(
			'map'  => $this->googlemaps->create_map()
		);
		
			// Load the page
		$data['title']="Maps Kurir";
		$data['name']= $this->m_maps->getAll('tb_jobdetail')->result();
		$data['detail']= $this->m_maps->cekMap($id)->row_array();
		$this->template->load('template','maps/index', $data);
	}


	function cekLogin(){
		$islogin=$this->session->userdata('is_login');
		$username=$this->session->userdata('username');
		$level=$this->session->userdata('level');

		if($this->session->userdata('is_login')==false){
			redirect('login');
		}else if("administrator"!=$this->session->userdata('level')){
			redirect('template','home/tidakada');
		}

	}

}
?>
